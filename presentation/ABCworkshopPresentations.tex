\documentclass{beamer}
\usepackage{./_style/beamerthemeElaine}
\graphicspath{{./_style/}}

\usepackage{lmodern}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{appendixnumberbeamer}
\usepackage{siunitx}
\usepackage{hyperref}
\usepackage[english]{babel}

%\usepackage{marvosym}

%\usepackage{tikz}
%\usetikzlibrary{calc}
%\newcommand\tikzmark[1]{%
%  \tikz[remember picture,overlay]\node (#1) {};%
%  }
  
%\newcommand\Connect[4][]{%
%\tikz[remember picture,overlay]
%  \draw[->,red,>=latex,#1] ( $ (#2.north east) + (5pt,0pt)$ ) -- ( $ (#3.north west) + (0pt,#4pt) $ );%
%  } %#1 is a optional argument, #2 is the start, #3 is the end

%\renewcommand{\vec}{\bold}

%for code
\usepackage{listings}
\lstset{language=Python,
    frame = tb,
    basicstyle=\normalsize\ttfamily,
    numbers=left,
    numberstyle=\tiny\color{gray}, 
    xleftmargin = 1em, 
    stepnumber=1,
	keywordstyle=\color{red},
	commentstyle=\color{gray},
	numberstyle=\tiny\color{gray},
	morecomment=[l]{!\ }% Comment only with space after !
}
\lstset{ %
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false
}

%margins for text
\mode<presentation>

%date is set to today, change if needed
\renewcommand{\elainedate}{\today}
\date{August 7, 2019}

%specify title and subtitle
\title{pyABC}
\subtitle{Workshop}

%customize		
\author[]{\fontfamily{qag}\fontsize{7}{6.0pt}\selectfont \textbf{Kai Budde}\\[0.15cm]
{ \fontfamily{qag}\fontsize{6.5}{5.5pt}\selectfont
Institute for Visual and Analytic Computing, University of Rostock
}}


\newenvironment{myitemize}{\setlength{\leftmargini}{1em}
\begin{itemize}}{\end{itemize}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Why pyABC?}

``pyABC~\cite{klinger2018pyabc} is a framework for distributed, likelihood-free inference. That means, if you have a model and some data and want to know the posterior distribution over the model parameters (i.e., you want to know which parameters explain with which probability the observed data) then pyABC might be for you.

\vspace{1em}
All you need is some way to numerically draw samples from the model, given the model parameters. pyABC “inverts” the model for you and tells you which parameters were well matching and which ones not. You do not need to analytically calculate the likelihood function.''

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]{Prerequisites}
\begin{myitemize}
\item Download and install Anaconda (\url{https://www.anaconda.com/distribution/}) with the latest Python 3.x version (at least Python 3.6)
\item Start Spyder and paste the following line into the IPython console and execute it
\end{myitemize}
\begin{lstlisting}
pip install --user pyabc #Or: pip install pyabc
\end{lstlisting}

\begin{myitemize}
\item Clone the git repository: \url{https://gitlab.elaine.uni-rostock.de/kb637/abcworkshop}
\item Optional: Start with a Python tutorial if you need one.
\end{myitemize}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Overview}
\tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Theoretical Background}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Statistical Inference}
\begin{frame}{Statistical Inference}

\begin{myitemize}
\item Process of using data analysis to deduce properties of an underlying probability distribution
\item Assumption that observed data set is sampled from a larger population
\item Statistical model is a set of assumptions concerning the generation of the observed data
\item Two main approaches:
\begin{enumerate}
\item Frequentist inference (e.g., hypothesis testing)
\item Bayesian inference (with Maximum-Likelihood as a special case)
\end{enumerate}
\item[$\rightarrow$] Check out \url{https://seeing-theory.brown.edu/} (A visual introduction to probability and statistics)
\end{myitemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Bayes' Theorem}
\begin{frame}{Bayes' Theorem}


\begin{equation}
p(\vartheta | D ) = \frac{p(D|\vartheta) \cdot p(\vartheta)}{p(D)}
\end{equation}

with the data $D$ and parameters $\vartheta$

\begin{equation*}
\text{posterior probability} = \frac{\text{likelihood} \cdot \text{prior probability}}{\text{evidence}}
\end{equation*}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Bayesian Inference}
\begin{frame}{Bayesian Inference}

\begin{myitemize}
\item Bayes' theorem:
\begin{equation*}
p(\vartheta | D ) = \frac{p(D|\vartheta) \cdot p(\vartheta)}{p(D)}
\end{equation*}

\item Bayesian inference:
\begin{itemize}
\item Treats $\vartheta$ as a random variable
\item Calculates (or approximates) the posterior probability distribution $p(\vartheta|D)$
\item Selection of the \textit{best} value for $\vartheta$ by looking at the posterior probability distribution is necessary (e.g., choose the expected value of $\vartheta$ assuming its variance is small enough)
\item Calculated variance of the parameter $\vartheta$ allows us to express our confidence in any specific value for it
\end{itemize}
\end{myitemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{ABC}
\begin{frame}{ABC}

\begin{myitemize}
\item Computation of likelihood is very expensive or even infeasible~\cite{yildirim2015parameter}
\item Approximate Bayesian Computation (ABC) circumvents calculating the likelihood by approximating the posterior
\end{myitemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Rejection Method}
\begin{frame}{Rejection Method}

\begin{myitemize}
\item Algorithm compares simulations ($D_i$) with the (experimental) data ($D$) and checks whether the difference between $D_i$ and $D$ is smaller than a given threshold ($\varepsilon$)
\item Population sizes defines how many samples need to be accepted -> stopping criterion
\end{myitemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ABC Rejection Algorithm}
\begin{frame}{ABC Rejection Algorithm}

\begin{minipage}[c]{7.5cm}
\begin{figure}
\includegraphics[width=\textwidth]{figures/ABCrejection}
\caption{Parameter estimation by Approximate Bayesian Computation: a conceptual overview.~\cite{sunnaaker2013approximate}}
\end{figure}
\end{minipage}%
\begin{minipage}{4.5cm}
\setlength{\leftmargini}{0.1em}
\begin{enumerate}
\item[] ABC rejection algorithm:
\item Sample $\theta_i$ from $p(\theta)$.
\item Generate the data set $D_i$ with the model $M_{\theta_i}$.
\item Compute the distance $\rho(S(D_i) , S(D))$ where $S()$ is some summary statistics if $D$ is of high dimension. If $\rho(S(D_i),S(D)) \leq \varepsilon$, then accept $\theta_i$, otherwise reject it.
\item Repeat steps 1 - 3 until there are enough parameters accepted.
\end{enumerate}

\end{minipage}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ABC Sequential Monte Carlo (SMC) Method}
\begin{frame}{ABC Sequential Monte Carlo (SMC) Method}

\begin{myitemize}
\item Approach the true posterior step-wise to circumvent small acceptance rates
\item Algorithm behaves in a cyclic manner in which an ABC rejection procedure is performed in every loop ($=$ every generation)
\end{myitemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ABC SMC Algorithm}
\begin{frame}{ABC SMC Algorithm}

\begin{minipage}[c]{6cm}
\begin{figure}
\includegraphics[width=\textwidth]{figures/ABCSMC}
\caption{Illustration of the ABC Sequential Monte Carlo method. The parameters are drawn from the proposal distribution of the third iteration.~\cite{lintusaari2017fundamentals}}
\end{figure}
\end{minipage}%
\begin{minipage}{6cm}
\setlength{\leftmargini}{1em}
\begin{enumerate}
\item[] ABC SMC algorithm:
\item Choose a threshold $\varepsilon$ that is big enough so that there will be a reasonable acceptance rate.
\item Sample from the prior $p(\theta)$.
\item After a fixed number of accepted samples, propose a new prior distribution $\tilde{p}(\theta)$.
\item Decrease $\varepsilon$.
\item Repeat sampling with new prior $\tilde{p}(\theta)$ and smaller $\varepsilon$ until fixed number of generations reached.
\end{enumerate}

\end{minipage}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{pyABC at Work}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{pyABC: Parameter Inference}
\begin{frame}{pyABC: Parameter Inference}

\begin{myitemize}
\item Open \url{https://gitlab.elaine.uni-rostock.de/kb637/abcworkshop/blob/master/scripts/1ParameterInference.py}
\item The script is based on \url{https://pyabc.readthedocs.io/en/latest/examples/parameter_inference.html}
\begin{itemize}
\item What happens when you change the standard deviation in the model?
\item What happens when you change the observation value?
\item What happens when the observation value is not within the prior?
\end{itemize}
\end{myitemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{pyABC: Parameter Inference for Physical Model}
\begin{frame}{pyABC: Parameter Inference for Physical Model}

\begin{myitemize}
\item We observe an accelerating car and record its positions at certain time points.
\item We assume a model of the form:
\begin{equation}
s(t) = 0.5 \cdot a \cdot t^2 + s0
\end{equation}
\item Open \url{https://gitlab.elaine.uni-rostock.de/kb637/abcworkshop/blob/master/scripts/2ParameterInference_PhysicalModel.py}
\begin{itemize}
\item What happens when you change the level of the measurement noise?
\item What happens when you take into account more time points?
\item What happens when you assume a model of a different form?
\end{itemize}
\end{myitemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{pyABC: Model Selection}
\begin{frame}{pyABC: Model selection}

\begin{myitemize}
\item Open \url{https://gitlab.elaine.uni-rostock.de/kb637/abcworkshop/blob/master/scripts/3ModelSelection.py}
\item The script is based on \url{https://pyabc.readthedocs.io/en/latest/examples/quickstart.html}
\end{myitemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Further Reading}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Further Reading}

\begin{myitemize}
\item Alternative package: ABCpy (\url{https://github.com/eth-cscs/abcpy})
\end{myitemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibframe{bibliography}




\end{document}
