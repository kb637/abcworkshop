# ABCworkshop

This is a repository for a mini workshop on Approximate Bayesian Computation and in particular on pyABC. The workshop was given for IRTG members of the CRC 1270 'Elaine' on August 7, 2019.

Feel free to contribute to the slides / scripts.