#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 13:52:15 2019

@author: kb (adapted fromhttps://pyabc.readthedocs.io/en/latest/examples/quickstart.html)
"""

import matplotlib
import os
import tempfile
import scipy.stats as st
import pyabc


#%% Defining a model

# Define a gaussian model (takes a single dictionary as input and returns a
# single dictionary as output)
sigma = .5
def model(parameters):
    # sample from a gaussian
    y = st.norm(parameters.x, sigma).rvs()
    # return the sample as dictionary
    return {"y": y}

#%% What it does:

# Get help:
print(st.norm.rvs.__doc__)

# draw a random sample from a normal distribution with \mu=0 and specified
# sigma (random_state makes sure that the numbers are reproducible)
dummy = st.norm(-1,2).rvs(size = 1, random_state = 1234)
n, bins, patches = matplotlib.pyplot.hist(dummy)

dummy = st.norm.rvs(size = 1000, random_state = 1234)
n, bins, patches = matplotlib.pyplot.hist(dummy, 10)

dummy = st.norm(-1,10).rvs(size = 100000, random_state = 1234)
n, bins, patches = matplotlib.pyplot.hist(dummy, 1000)

dummy = st.norm(-1,1).rvs(size = 100000, random_state = 1234)
n, bins, patches = matplotlib.pyplot.hist(dummy, 1000)

dummy = st.norm(-1,10).rvs(size = 100000, random_state = 1234)
n, bins, patches = matplotlib.pyplot.hist(dummy, 1000)


# Change the mean and standard deviation
print(st.norm.__doc__)
# -> rvs are random variates
# .> ``loc`` specifies the mean, ``scale`` specifies the standard deviation

dummy = st.norm(sigma)


#%% 


 
# For model selection we usually have more than one model.
# These are assembled in a list. We require a Bayesian prior over the models.
# The default is to have a uniform prior over the model classes.
# This concludes the model definition.


# We define two models, but they are identical so far
models = [model, model]

# However, our models' priors are not the same.
# Their mean differs.
mu_x_1 = 0
mu_x_2 = 1

parameter_priors = [
        pyabc.Distribution(x=pyabc.RV("norm", mu_x_1, sigma)),
        pyabc.Distribution(x=pyabc.RV("norm", mu_x_2, sigma))
        ]


#%% Configuring the ABCSMC run

# We plug all the ABC options together
abc = pyabc.ABCSMC(
        models, parameter_priors,
        pyabc.PercentileDistance(measures_to_use=["y"])
        )

#%% Setting the observed data

# y_observed is the important piece here: our actual observation.
y_observed = 1
# and we define where to store the results
db_path = ("sqlite:///" +
           os.path.join(tempfile.gettempdir(), "test.db"))
abc_id = abc.new(db_path, {"y": y_observed})

print("ABC-SMC run ID:", abc_id)

#%% Running the ABC

# We run the ABC until either criterion is met
history = abc.run(minimum_epsilon=0.2, max_nr_populations=5)
#history is abc.history

#%% Evaluate and visualize the results

# Evaluate the model probabililties
model_probabilities = history.get_model_probabilities()
print(model_probabilities)

pyabc.visualization.plot_model_probabilities(history)


