# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 22:45:53 2019

@author: Kai
"""


import pyabc

import scipy as sp
import tempfile
import os

#%% Model

# Our model is a model of the distance s of an accelerating car:
# s = 0.5*a*t^2 + s0
# Both a and s0 are error-prone.

def model(parameters):
    return {"data": 0.5 * parameters["a"] *
            measurement_times * measurement_times + parameters["s0"]}


#%% Data

measurement_times = sp.array([0.0, 1.0, 3.0, 10.0, 20.0])
a = 0.5 # in m/s/s
s0 = 10 # in m
measurement_data =  (0.5 * (a + a / 10 * sp.randn()) *
                     measurement_times * measurement_times +
                     (s0 + s0 / 10 * sp.randn())
                     )

#%% Prior for a and s0

parameter_prior = pyabc.Distribution(a  = pyabc.RV("uniform", 0, 1),
                                     s0 = pyabc.RV("uniform", 0, 20))

#%% Distance function
# We need now a distance function for all data points
def distance(x, y):
    return sp.absolute(x["data"] - y["data"]).sum()


#%% Creat ABCSMC Object

abc = pyabc.ABCSMC(models = model,
                   parameter_priors = parameter_prior,
                   distance_function = distance)

db_path = ("sqlite:///" +
           os.path.join(tempfile.gettempdir(), "test.db"))

abc.new(db_path, {"data": measurement_data})

#%% Start sampling

history = abc.run(minimum_epsilon = 0.1,
                  max_nr_populations = 30)

# You can repeat this and it starts where it ended!

#%% Visualizing the results

#for t in range(history.max_t+1):
for t in range(5):
    ax = pyabc.visualization.plot_kde_2d(*history.get_distribution(m=0, t=t),
                                         "a", "s0",
                                         xmin=0, xmax=1, numx=300,
                                         ymin=0, ymax=20, numy=300)
    ax.scatter([a], [s0],
               color="C1",
               label='True Parameters = {:.3f}, {:.3f}'.format(a, s0))
    ax.set_title("Posterior t={}".format(t))
    ax.legend()
    

#%% Visualizing more results

pyabc.visualization.plot_epsilons(history)

